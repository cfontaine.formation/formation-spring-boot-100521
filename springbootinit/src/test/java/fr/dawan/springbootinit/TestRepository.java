package fr.dawan.springbootinit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import fr.dawan.springbootinit.Controllers.ProduitController;
import fr.dawan.springbootinit.entities.Produit;
import fr.dawan.springbootinit.repositories.ProduitRepository;
//@ActiveProfiles("TEST")
@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
class TestRepository {
    
    @Autowired 
    private ProduitController produitController;
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired 
    private ProduitRepository produitRepository;
    
    
    @Test
    void test() {
        assertThat(produitController).isNotNull();
    }
    
    @Test
    @DisplayName("Test de /api/produits et Get findAll")
    void testFindAll() {
        try {
            mockMvc.perform(get("/api/produits")).andExpect(status().isOk())
            .andExpect(jsonPath("$[0].description",is("Ecran")))
            .andExpect(jsonPath("$[0].prix",is(200.0)));
        } catch (Exception e) {
           assertTrue(false);
       }
    }
    
    @Test
    void testProduitRepositorySave() {
        Produit p=new Produit();
        p.setDescription("test");
        p.setPrix(10.0);
        Produit res=produitRepository.saveAndFlush(p);
        assertNotEquals(0, res.getId());
    }

}
