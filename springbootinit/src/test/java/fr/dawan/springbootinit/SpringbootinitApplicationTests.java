package fr.dawan.springbootinit;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.dawan.springbootinit.entities.Produit;
import fr.dawan.springbootinit.repositories.ProduitRepository;

@SpringBootTest
class SpringbootinitApplicationTests {

    @Autowired
    ProduitRepository produitRepository;
    
	@Test
	void contextLoads() {
	       
	    Page<Produit> page=produitRepository.findByPrixLessThan(2000.0, PageRequest.of(1, 2));
	    System.out.println(page.getNumber());
	    System.out.println(page.getTotalElements());
	    System.out.println(page.getTotalPages());
	    List<Produit> lst=produitRepository.findFirst3ByPrixLessThanOrderByPrix(300.0);
	    for(Produit prod: lst) {
	        System.out.println(prod);
	    }
	    
	    
	}

}
