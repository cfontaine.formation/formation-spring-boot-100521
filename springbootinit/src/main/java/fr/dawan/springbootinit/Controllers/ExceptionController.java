package fr.dawan.springbootinit.Controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler{
    
    @ExceptionHandler(Exception.class)
    public String handleException(Exception e) {
        System.out.println("Exception Globale");
        return "Global ";
    }

}
