package fr.dawan.springbootinit.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    
//    @Autowired
//    ConfigValue application;
    
//    @Value("${configure}")
//    private String testValue ;
    
    @RequestMapping("/")
    public String HelloWorld() {
      return "Hello world";
        //  return application.getConfig();
    }
    
}
