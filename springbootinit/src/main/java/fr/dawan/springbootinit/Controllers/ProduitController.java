package fr.dawan.springbootinit.Controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springbootinit.entities.Produit;
import fr.dawan.springbootinit.repositories.ProduitRepository;

@RestController
@RequestMapping("api/produits")
public class ProduitController {
    @Autowired
    ProduitRepository produitRepository;
   
    
    //@RequestMapping(value="url", method=RequestMethod.GET)

    @GetMapping(produces = "application/json")
    public List<Produit> getAll() {
       return produitRepository.findAll();
    }
    
    @GetMapping(value="/{id}",produces = "application/json")
    public Produit getById(@PathVariable long id) {
        return produitRepository.findById(id).get();
    }
    
   @GetMapping(params= {"page","size"},produces = "application/json")
   public List<Produit> getPage(@RequestParam int page, @RequestParam int size) {
       return produitRepository.findAll(PageRequest.of(page, size)).getContent();
   }
   
   @PostMapping(consumes= "application/json",produces = "application/json")
   public Produit save(@RequestBody Produit prod) {
       return produitRepository.saveAndFlush(prod);
   }
   
   @PutMapping(consumes= "application/json",produces = "application/json")
   public Produit update(@RequestBody Produit prod) {
       return produitRepository.saveAndFlush(prod);
   }
   
   @DeleteMapping(value="/{id}")
   public ResponseEntity<String> remove(@PathVariable long id) {

       try {
        produitRepository.deleteById(id);
       }
       catch(Exception e) {
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
       }
       return new ResponseEntity<>(HttpStatus.ACCEPTED);
   }
   
   @ResponseStatus(HttpStatus.NOT_FOUND)
   @ExceptionHandler(IOException.class)
   public String  handleException(IOException e) {
       System.out.println("Gestion IoException Controler Produit");
       return "IoException "+ e.getMessage();
   }
   
   
   @GetMapping("/ioexception")
   public void testIoException() throws IOException {
       throw new IOException("=> IoException");
   }
   
   @GetMapping("/exception")
   public void testException() throws Exception {
       throw new Exception("=> Exception");
   }
}
