package fr.dawan.springbootinit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class SpringbootinitApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SpringbootinitApplication.class);
        // app.setAddCommandLineProperties(false);
        app.run(args);
    }

}

