package fr.dawan.springbootinit.authprovider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import fr.dawan.springbootinit.services.UserService;

public class appAuthProvide extends DaoAuthenticationProvider {

    
    @Autowired
    private UserService userService;
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
       UsernamePasswordAuthenticationToken authtoken =(UsernamePasswordAuthenticationToken) authentication;
       String name=authtoken.getName();
       String password=authtoken.getCredentials().toString();
      UserDetails user=userService.loadUserByUsername(name);
      if(user==null) {
          throw new BadCredentialsException("BadCredential");
      }
       return new UsernamePasswordAuthenticationToken(user,null,user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

}
