package fr.dawan.springbootinit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springbootinit.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {

}
