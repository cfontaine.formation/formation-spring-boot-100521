package fr.dawan.springbootinit.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.springbootinit.entities.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

    List<Produit>findByPrixLessThan(double prix);
    List<Produit>findByPrixBetweenOrderByPrixDesc(double prixmin,double prixmax);
    
    List<Produit> findByPrixGreaterThanAndDescriptionLike(double prix,String description);

    @Query(value="SELECT p FROM Produit p JOIN p.marque m WHERE m.nom=:marque")
    List<Produit> findByMarqueNom(@Param("marque")String marque);
    
    @Query(nativeQuery = true,value="SELECT * FROM produits JOIN marques ON produits.marque_id=marques.id WHERE marques.nom=:marque")
    List<Produit> findByMarqueNomSQL(@Param("marque")String marque);

    Page<Produit>findByPrixLessThan(double prix,Pageable page);
    
    List<Produit>findFirst3ByPrixLessThanOrderByPrix(double prix);
}
