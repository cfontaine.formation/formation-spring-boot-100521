package springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBootApp {
    
    public static void main(String[] args) {
    ApplicationContext ctx= SpringApplication.run(SpringBootApp.class, args);
    String str[]=ctx.getBeanDefinitionNames();
    for(String s: str) {
        System.out.println(s);
    }
    }

}
