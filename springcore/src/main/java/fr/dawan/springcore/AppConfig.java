package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.springcore.beans.Client;
import fr.dawan.springcore.beans.Service;
import fr.dawan.springcore.beans.ServiceImpl;

@Configuration
@ComponentScan(basePackages = "fr.dawan.springcore")
public class AppConfig {
    
    @Bean
    public Client client1() {
        return new Client();
    }
    
//    @Bean
//    public Service service1() {
//        return new ServiceImpl();
//    }
//
//    @Bean
//    public Service service2() {
//        return new ServiceImpl();
//    }
}
