package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.dawan.springcore.beans.Client;

public class Main {

    public static void main(String[] args) {
        ApplicationContext ctx= new AnnotationConfigApplicationContext(AppConfig.class);
        Client c1=ctx.getBean("client1",Client.class);
        c1.doSomething();
    }

}
