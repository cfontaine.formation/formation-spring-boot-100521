package fr.dawan.springcore.beans;

import org.springframework.stereotype.Component;

@Component
public class ServiceImpl implements Service {

    @Override
    public String getInfo() {
        
        return "Info Service";
    }

}
