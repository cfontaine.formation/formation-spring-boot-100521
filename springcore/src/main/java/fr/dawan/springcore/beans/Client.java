package fr.dawan.springcore.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public class Client {
    
    @Autowired
 //   @Qualifier("service2")
    private Service service;

    public Client() {
        
    }
    
    
    public Client(Service service) {
        this.service = service;
    }


    public void doSomething() {
        System.out.println("Dosomething");
       System.out.println( service.getInfo());
        }
    
}
