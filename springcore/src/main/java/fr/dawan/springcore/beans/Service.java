package fr.dawan.springcore.beans;

public interface Service {
    String getInfo();
}
